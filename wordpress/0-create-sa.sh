#!/usr/local/bin/bash
gcloud iam service-accounts create $SA_NAME --display-name $SA_NAME
gcloud projects add-iam-policy-binding $PROJECT_ID --member serviceAccount:$FULL_SA_NAME --role roles/cloudsql.client

# setup workload identity between KSA and GSA
kubectl create sa $SA_NAME
gcloud iam service-accounts add-iam-policy-binding --role roles/iam.workloadIdentityUser \
  --member "serviceAccount:$PROJECT_ID.svc.id.goog[default/$SA_NAME]" $FULL_SA_NAME
kubectl annotate serviceaccount $SA_NAME iam.gke.io/gcp-service-account=$FULL_SA_NAME

kubectl create sa $ADMIN_NAME
gcloud iam service-accounts add-iam-policy-binding --role roles/iam.workloadIdentityUser \
  --member "serviceAccount:$PROJECT_ID.svc.id.goog[default/$ADMIN_NAME]" $FULL_ADMIN_SA_NAME
kubectl annotate serviceaccount $ADMIN_NAME iam.gke.io/gcp-service-account=$FULL_ADMIN_SA_NAME
