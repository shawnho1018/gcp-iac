export SQLADMIN="user"
export DBNAME="wordpress"
export REGIONNAME="asia-east1"
export SQLADMINPWD="sqladmin"
export SQLNAME="wordpress-sql-$(date +%s)"
export PROJECT_ID="anthos-demo-280104"
export SA_NAME="cloudsql-client"
export FULL_SA_NAME="$SA_NAME"@"$PROJECT_ID".iam.gserviceaccount.com
export ADMIN_NAME="gke-admin"
#export CONNECTION=$(gcloud sql instances describe $SQLNAME --format="value(connectionName)")
