#!/usr/local/bin/bash
kubectl delete -f wordpress.yaml
kubectl delete -f wordpress-svc.yaml
kubectl delete -f k8s-mysql-credential.yaml

kubectl delete -f k8s-mysql-credential.yaml -n config-connector 
kubectl delete -f mysqlUser.yaml -n config-connector
kubectl delete -f mysqlDB.yaml -n config-connector
kubectl delete -f mysqlInstance.yaml -n config-connector
