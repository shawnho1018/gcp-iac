#!/usr/local/bin/bash
# Create CloudSQL instance and the corresponding database
kubectl apply -f mysqlInstance.yaml -n config-connector
kubectl apply -f mysqlDB.yaml -n config-connector
kubectl apply -f k8s-mysql-credential.yaml -n config-connector
kubectl apply -f mysqlUser.yaml -n config-connector

