#!/usr/local/bin/bash
COUNTER=61
until [  $COUNTER -lt 2 ]; do
  echo "Waiting for instance to finish starting.."
  result=$(kubectl logs $CNRM -n cnrm-system -c manager | grep $SQLNAME \
     | grep "successfully finished reconcile")
  if [ ! -z "$result" ]; then
    break
  fi
  sleep 10
  COUNTER=$(( COUNTER - 1 ))
done

if [[ $COUNTER -lt 2 ]]; then
  echo "Cloud SQL instance creation timed out"
  exit 1
fi
