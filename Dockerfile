FROM alpine:latest
# This image provides git, kpt, and kubectl
ENV KUBE_LATEST_VERSION="v1.19.2"

RUN apk add --update ca-certificates \
 && apk add --update -t deps curl \
 && apk add --update wget git \
 && curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
 && chmod +x /usr/local/bin/kubectl \
 && wget https://storage.googleapis.com/kpt-dev/latest/linux_amd64/kpt -O /usr/local/bin/kpt \
 && chmod +x /usr/local/bin/kpt \
 && apk del --purge deps \
 && rm /var/cache/apk/*

WORKDIR /root
CMD ["/bin/sh"]
